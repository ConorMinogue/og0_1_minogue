package tester;

public abstract class TTest {

	protected float Ergebnis;
	protected boolean Aktiv;
	protected TCanvas Canvas;
	protected TRect Rechteck;
	protected int xMitte;
	protected int yMitte;

	public TTest(TCanvas Canvas, TRect Rechteck) {
		super();
		this.Canvas = Canvas;
		this.Rechteck = Rechteck;
	}
	
	public TTest(float ergebnis, boolean aktiv, TCanvas canvas, TRect rechteck, int xMitte, int yMitte) {
		super();
		Ergebnis = ergebnis;
		Aktiv = aktiv;
		Canvas = canvas;
		Rechteck = rechteck;
		this.xMitte = xMitte;
		this.yMitte = yMitte;
	}


	protected void warten() {

	}

	public float getErgebnis() {
		return this.Ergebnis;
	}

	public boolean isAktiv() {
		return this.Aktiv;
	}

	public abstract void starten();

	public abstract void stoppen(char Taste);

	public abstract String zeigeHilfe();
}
