package tester;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.BorderLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JEditorPane;
import javax.swing.JRadioButton;
import javax.swing.JLabel;
import java.awt.Color;

public class TestGUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TestGUI frame = new TestGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TestGUI() {
		setTitle("Tauglichkeitstester");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JPanel pnlMain = new JPanel();
		contentPane.add(pnlMain, BorderLayout.CENTER);
		pnlMain.setLayout(new BorderLayout(0, 0));

		JPanel pnlCenter = new JPanel();
		pnlMain.add(pnlCenter, BorderLayout.CENTER);
		pnlCenter.setLayout(new BorderLayout(0, 0));

		JPanel pnlWest_1 = new JPanel();
		pnlWest_1.setBackground(new Color(255, 255, 255));
		pnlCenter.add(pnlWest_1, BorderLayout.WEST);

		JLabel lblSpacer = new JLabel("                                                                            ");
		lblSpacer.setBackground(new Color(255, 255, 255));
		pnlWest_1.add(lblSpacer);

		JPanel pnlEast_1 = new JPanel();
		pnlCenter.add(pnlEast_1, BorderLayout.EAST);
		pnlEast_1.setLayout(new BorderLayout(0, 0));

		JTextArea textArea = new JTextArea();
		textArea.setColumns(30);
		pnlEast_1.add(textArea, BorderLayout.CENTER);

		JPanel pnlSouth = new JPanel();
		pnlMain.add(pnlSouth, BorderLayout.SOUTH);
		pnlSouth.setLayout(new BorderLayout(0, 0));

		JPanel pnlWest_2 = new JPanel();
		pnlSouth.add(pnlWest_2, BorderLayout.WEST);
		pnlWest_2.setLayout(new BorderLayout(0, 0));

		JLabel lblTest = new JLabel("Test");
		pnlWest_2.add(lblTest, BorderLayout.NORTH);

		JPanel pnlRadiobuttons = new JPanel();
		pnlWest_2.add(pnlRadiobuttons, BorderLayout.CENTER);
		pnlRadiobuttons.setLayout(new BorderLayout(0, 0));

		ButtonGroup btnGroup = new ButtonGroup();

		JRadioButton rdbtnReaktion = new JRadioButton("Reaktion");
		pnlRadiobuttons.add(rdbtnReaktion, BorderLayout.NORTH);
		btnGroup.add(rdbtnReaktion);

		JRadioButton rdbtnEinschaetzung = new JRadioButton("Einsch\u00E4tzung");
		pnlRadiobuttons.add(rdbtnEinschaetzung, BorderLayout.CENTER);
		btnGroup.add(rdbtnEinschaetzung);

		JRadioButton rdbtnKonzentration = new JRadioButton("Konzentration");
		pnlRadiobuttons.add(rdbtnKonzentration, BorderLayout.SOUTH);
		btnGroup.add(rdbtnKonzentration);

		JPanel pnlEast_2 = new JPanel();
		pnlSouth.add(pnlEast_2, BorderLayout.EAST);
		pnlEast_2.setLayout(new BorderLayout(0, 0));

		JButton btnStart = new JButton("Start");
		pnlEast_2.add(btnStart, BorderLayout.WEST);

		JButton btnStop = new JButton("Stopp");
		pnlEast_2.add(btnStop, BorderLayout.EAST);

		JPanel pnlNorth = new JPanel();
		pnlMain.add(pnlNorth, BorderLayout.NORTH);
	}

}
