package tester;

public class TEinschaetzung extends TTest {

	protected TBallon Ballon;
	protected TRect Quadrat;
	
	public TEinschaetzung(TCanvas Canvas, TRect Rechteck, TBallon ballon, TRect quadrat) {
		super(Canvas, Rechteck);
		Ballon = ballon;
		Quadrat = quadrat;
	}

	@override
	public String zeigeHilfe() {
		
	}
	
	override
	public void starten() {
		this.Aktiv = true;
		
	}
	
	override
	public void stoppen(char Taste) {
		
	}
}
