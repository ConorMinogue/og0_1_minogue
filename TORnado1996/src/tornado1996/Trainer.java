package tornado1996;

public class Trainer extends Person {

	private char lizenzklasse;
	private char spielklasse;
	private int entschaedigung;

	public Trainer(String name, String telefonnummer, boolean jahresbeitragBezahlt, char lizenzklasse, char spielklasse,
			int entschaedigung) {
		super(name, telefonnummer, jahresbeitragBezahlt);
		this.lizenzklasse = lizenzklasse;
		this.spielklasse = spielklasse;
		this.entschaedigung = entschaedigung;
	}

	public char getLizenzklasse() {
		return lizenzklasse;
	}

	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}

	public char getSpielklasse() {
		return spielklasse;
	}

	public void setSpielklasse(char spielklasse) {
		this.spielklasse = spielklasse;
	}

	public int getEntschaedigung() {
		return entschaedigung;
	}

	public void setEntschaedigung(int entschaedigung) {
		this.entschaedigung = entschaedigung;
	}

}
