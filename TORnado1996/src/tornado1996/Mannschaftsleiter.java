package tornado1996;

public class Mannschaftsleiter extends Spieler {

	private String mannschaftsName;
	private int rabatt;

	public Mannschaftsleiter(String name, String telefonnummer, boolean jahresbeitragBezahlt, int trikotnummer,
			String spielposition, String mannschaftsName, int rabatt) {
		super(name, telefonnummer, jahresbeitragBezahlt, trikotnummer, spielposition);
		this.mannschaftsName = mannschaftsName;
		this.rabatt = rabatt;
	}

	public String getMannschaftsName() {
		return mannschaftsName;
	}

	public void setMannschaftsName(String mannschaftsName) {
		this.mannschaftsName = mannschaftsName;
	}

	public int getRabatt() {
		return rabatt;
	}

	public void setRabatt(int rabatt) {
		this.rabatt = rabatt;
	}

}
