package tornado1996;

public class TestKickers {

	public static void main(String[] args) {
		Spieler sp1 = new Spieler("Peter Pan", "0176 19248128", true, 7, "St�rmer");
		Mannschaftsleiter ml1 = new Mannschaftsleiter("Manuel M�ller", "0123 6234094", false, 2, "Verteidiger", "OSZ Kickers", 20);
		Trainer tr1 = new Trainer("Ulli Ulrich", "1612 35190322", true, 'b', 'b', 250);
		Schiedsrichter sr1 = new Schiedsrichter("Barbara Bauer", "1532 3589123894", false, 37);

		System.out.println(sp1.getName());
		System.out.println(sp1.getTelefonnummer());
		System.out.println(sp1.isJahresbeitragBezahlt());
		System.out.println(sp1.getTrikotnummer());
		System.out.println(sp1.getSpielposition());
		
		System.out.println("-----");
		
		System.out.println(ml1.getName());
		System.out.println(ml1.getTelefonnummer());
		System.out.println(ml1.isJahresbeitragBezahlt());
		System.out.println(ml1.getTrikotnummer());
		System.out.println(ml1.getSpielposition());
		System.out.println(ml1.getMannschaftsName());
		System.out.println(ml1.getRabatt());
		
		System.out.println("-----");
		
		System.out.println(tr1.getName());
		System.out.println(tr1.getTelefonnummer());
		System.out.println(tr1.isJahresbeitragBezahlt());
		System.out.println(tr1.getLizenzklasse());
		System.out.println(tr1.getSpielklasse());
		System.out.println(tr1.getEntschaedigung());
		
		System.out.println("-----");
		
		System.out.println(sr1.getName());
		System.out.println(sr1.getTelefonnummer());
		System.out.println(sr1.isJahresbeitragBezahlt());
		System.out.println(sr1.getAnzahlSpiele());
		
	}

}
