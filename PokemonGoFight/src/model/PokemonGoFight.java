package model;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.FlowLayout;
import java.awt.Dimension;

public class PokemonGoFight extends JFrame {

	private JPanel pnlHintergrund;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PokemonGoFight frame = new PokemonGoFight();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PokemonGoFight() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 600);
		pnlHintergrund = new JPanel();
		pnlHintergrund.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pnlHintergrund);
		pnlHintergrund.setLayout(null);
		
		JPanel pnlPokemon1 = new JPanel();
		pnlPokemon1.setMinimumSize(new Dimension(200, 10));
		pnlPokemon1.setBounds(119, 10, 196, 551);
		pnlHintergrund.add(pnlPokemon1);
		pnlPokemon1.setLayout(new BorderLayout(0, 0));
		
		JPanel pnlNorth1 = new JPanel();
		pnlPokemon1.add(pnlNorth1, BorderLayout.NORTH);
		pnlNorth1.setLayout(new BorderLayout(0, 0));
		
		JLabel lblWettkampfpunkte1 = new JLabel("WP");
		pnlNorth1.add(lblWettkampfpunkte1, BorderLayout.NORTH);
		
		JLabel lblBild1 = new JLabel("Bild");
		pnlNorth1.add(lblBild1, BorderLayout.CENTER);
		
		JPanel pnlSouth1 = new JPanel();
		pnlPokemon1.add(pnlSouth1, BorderLayout.SOUTH);
		pnlSouth1.setLayout(new BorderLayout(0, 0));
		
		JPanel pnlAttacken = new JPanel();
		pnlSouth1.add(pnlAttacken, BorderLayout.NORTH);
		pnlAttacken.setLayout(new GridLayout(2, 2, 0, 0));
		
		JLabel lblAngriff1_1 = new JLabel("Stahlfl\u00FCgel");
		pnlAttacken.add(lblAngriff1_1);
		
		JLabel lblAngriff1_1Schaden1 = new JLabel("15");
		pnlAttacken.add(lblAngriff1_1Schaden1);
		
		JLabel lblAngriff1_2 = new JLabel("Hyperstrahl");
		pnlAttacken.add(lblAngriff1_2);
		
		JLabel lblAngriff1_2Schaden = new JLabel("70");
		pnlAttacken.add(lblAngriff1_2Schaden);
		
		JLabel lblDatum1 = new JLabel("26.07.2016");
		pnlSouth1.add(lblDatum1, BorderLayout.SOUTH);
		
		JPanel pnlCenter1 = new JPanel();
		pnlPokemon1.add(pnlCenter1, BorderLayout.CENTER);
		pnlCenter1.setLayout(new BorderLayout(0, 0));
		
		JPanel pnlSouth1_1 = new JPanel();
		pnlCenter1.add(pnlSouth1_1, BorderLayout.SOUTH);
		GridBagLayout gbl_pnlSouth1_1 = new GridBagLayout();
		gbl_pnlSouth1_1.columnWidths = new int[]{0, 0};
		gbl_pnlSouth1_1.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_pnlSouth1_1.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_pnlSouth1_1.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		pnlSouth1_1.setLayout(gbl_pnlSouth1_1);
		
		JLabel lblTypBeschr1 = new JLabel("Gestein / Flug");
		GridBagConstraints gbc_lblTypBeschr1 = new GridBagConstraints();
		gbc_lblTypBeschr1.insets = new Insets(0, 0, 5, 0);
		gbc_lblTypBeschr1.gridx = 0;
		gbc_lblTypBeschr1.gridy = 0;
		pnlSouth1_1.add(lblTypBeschr1, gbc_lblTypBeschr1);
		
		JLabel lblGewichtBeschr1 = new JLabel("79.05 kg");
		GridBagConstraints gbc_lblGewichtBeschr1 = new GridBagConstraints();
		gbc_lblGewichtBeschr1.insets = new Insets(0, 0, 5, 0);
		gbc_lblGewichtBeschr1.gridx = 1;
		gbc_lblGewichtBeschr1.gridy = 0;
		pnlSouth1_1.add(lblGewichtBeschr1, gbc_lblGewichtBeschr1);
		
		JLabel lblGroesseBeschr1 = new JLabel("2.03m");
		GridBagConstraints gbc_lblGroesseBeschr1 = new GridBagConstraints();
		gbc_lblGroesseBeschr1.insets = new Insets(0, 0, 5, 0);
		gbc_lblGroesseBeschr1.gridx = 2;
		gbc_lblGroesseBeschr1.gridy = 0;
		pnlSouth1_1.add(lblGroesseBeschr1, gbc_lblGroesseBeschr1);
		
		JLabel lblTyp1 = new JLabel("Typ");
		GridBagConstraints gbc_lblTyp1 = new GridBagConstraints();
		gbc_lblTyp1.insets = new Insets(0, 0, 5, 0);
		gbc_lblTyp1.gridx = 0;
		gbc_lblTyp1.gridy = 1;
		pnlSouth1_1.add(lblTyp1, gbc_lblTyp1);
		
		JLabel lblGewicht1 = new JLabel("Gewicht");
		GridBagConstraints gbc_lblGewicht1 = new GridBagConstraints();
		gbc_lblGewicht1.insets = new Insets(0, 0, 5, 0);
		gbc_lblGewicht1.gridx = 1;
		gbc_lblGewicht1.gridy = 1;
		pnlSouth1_1.add(lblGewicht1, gbc_lblGewicht1);
		
		JLabel lblGroesse1 = new JLabel("Gr\u00F6\u00DFe");
		GridBagConstraints gbc_lblGroesse1 = new GridBagConstraints();
		gbc_lblGroesse1.gridx = 2;
		gbc_lblGroesse1.gridy = 1;
		pnlSouth1_1.add(lblGroesse1, gbc_lblGroesse1);
		
		JPanel pnlCenter1_1 = new JPanel();
		pnlCenter1.add(pnlCenter1_1, BorderLayout.CENTER);
		GridBagLayout gbl_pnlCenter1_1 = new GridBagLayout();
		gbl_pnlCenter1_1.columnWidths = new int[]{146, 0};
		gbl_pnlCenter1_1.rowHeights = new int[]{50, 50, 50, 0};
		gbl_pnlCenter1_1.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_pnlCenter1_1.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		pnlCenter1_1.setLayout(gbl_pnlCenter1_1);
		
		JLabel lblName1 = new JLabel("Aerodactyl");
		GridBagConstraints gbc_lblName1 = new GridBagConstraints();
		gbc_lblName1.fill = GridBagConstraints.BOTH;
		gbc_lblName1.insets = new Insets(0, 0, 5, 0);
		gbc_lblName1.gridx = 0;
		gbc_lblName1.gridy = 0;
		pnlCenter1_1.add(lblName1, gbc_lblName1);
		
		JProgressBar prbAnzeigeKP1 = new JProgressBar();
		GridBagConstraints gbc_prbAnzeigeKP1 = new GridBagConstraints();
		gbc_prbAnzeigeKP1.fill = GridBagConstraints.BOTH;
		gbc_prbAnzeigeKP1.insets = new Insets(0, 0, 5, 0);
		gbc_prbAnzeigeKP1.gridx = 0;
		gbc_prbAnzeigeKP1.gridy = 1;
		pnlCenter1_1.add(prbAnzeigeKP1, gbc_prbAnzeigeKP1);
		
		JLabel lblKP1 = new JLabel("KP 52 / 52");
		GridBagConstraints gbc_lblKP1 = new GridBagConstraints();
		gbc_lblKP1.fill = GridBagConstraints.BOTH;
		gbc_lblKP1.gridx = 0;
		gbc_lblKP1.gridy = 2;
		pnlCenter1_1.add(lblKP1, gbc_lblKP1);
	}

}
