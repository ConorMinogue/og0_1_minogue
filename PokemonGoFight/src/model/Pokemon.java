package model;

import java.util.Arrays;

public class Pokemon {

	private int nummer;
	private int wettkampfpunkte;
	private boolean favorit;
	private String bild;
	private String art;
	private String name;
	private int maximaleKP;
	private int aktuelleKP;
	private String typ;
	private double gewicht;
	private double groesse;
	private String funddatum;
	private Attacke[] attacken;

	public Pokemon() {

	}

	public Pokemon(int nummer, int wettkampfpunkte, boolean favorit, String bild, String art, String name,
			int maximaleKP, int aktuelleKP, String typ, double gewicht, double groesse, String funddatum,
			Attacke[] attacken) {
		super();
		this.nummer = nummer;
		this.wettkampfpunkte = wettkampfpunkte;
		this.favorit = favorit;
		this.bild = bild;
		this.art = art;
		this.name = name;
		this.maximaleKP = maximaleKP;
		this.aktuelleKP = aktuelleKP;
		this.typ = typ;
		this.gewicht = gewicht;
		this.groesse = groesse;
		this.funddatum = funddatum;
		this.attacken = attacken;
	}

	public int getNummer() {
		return nummer;
	}

	public void setNummer(int nummer) {
		this.nummer = nummer;
	}

	public int getWettkampfpunkte() {
		return wettkampfpunkte;
	}

	public void setWettkampfpunkte(int wettkampfpunkte) {
		this.wettkampfpunkte = wettkampfpunkte;
	}

	public boolean isFavorit() {
		return favorit;
	}

	public void setFavorit(boolean favorit) {
		this.favorit = favorit;
	}

	public String getBild() {
		return bild;
	}

	public void setBild(String bild) {
		this.bild = bild;
	}

	public String getArt() {
		return art;
	}

	public void setArt(String art) {
		this.art = art;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMaximaleKP() {
		return maximaleKP;
	}

	public void setMaximaleKP(int maximaleKP) {
		this.maximaleKP = maximaleKP;
	}

	public int getAktuelleKP() {
		return aktuelleKP;
	}

	public void setAktuelleKP(int aktuelleKP) {
		this.aktuelleKP = aktuelleKP;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public double getGewicht() {
		return gewicht;
	}

	public void setGewicht(double gewicht) {
		this.gewicht = gewicht;
	}

	public double getGroesse() {
		return groesse;
	}

	public void setGroesse(double groesse) {
		this.groesse = groesse;
	}

	public String getFunddatum() {
		return funddatum;
	}

	public void setFunddatum(String funddatum) {
		this.funddatum = funddatum;
	}

	public Attacke[] getAttacken() {
		return attacken;
	}

	public void setAttacken(Attacke[] attacken) {
		this.attacken = attacken;
	}

	@Override
	public String toString() {
		return "Pokemon [nummer=" + nummer + ", wettkampfpunkte=" + wettkampfpunkte + ", favorit=" + favorit + ", bild="
				+ bild + ", art=" + art + ", name=" + name + ", maximaleKP=" + maximaleKP + ", aktuelleKP=" + aktuelleKP
				+ ", typ=" + typ + ", gewicht=" + gewicht + ", groesse=" + groesse + ", funddatum=" + funddatum
				+ ", attacken=" + Arrays.toString(attacken) + "]";
	}

}
