package model;

public class Account {

	private String nickname;
	private int anzahlSternstaub;
	private int anzahlBonos;

	public Account() {
	}

	public Account(String nickname, int anzahlSternstaub, int anzahlBonos) {
		super();
		this.nickname = nickname;
		this.anzahlSternstaub = anzahlSternstaub;
		this.anzahlBonos = anzahlBonos;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public int getAnzahlSternstaub() {
		return anzahlSternstaub;
	}

	public void setAnzahlSternstaub(int anzahlSternstaub) {
		this.anzahlSternstaub = anzahlSternstaub;
	}

	public int getAnzahlBonos() {
		return anzahlBonos;
	}

	public void setAnzahlBonos(int anzahlBonos) {
		this.anzahlBonos = anzahlBonos;
	}

	@Override
	public String toString() {
		return "Account [nickname=" + nickname + ", anzahlSternstaub=" + anzahlSternstaub + ", anzahlBonos="
				+ anzahlBonos + "]";
	}

}
