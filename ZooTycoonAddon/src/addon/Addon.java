package addon;

public class Addon {

	private int idnummer;
	private String bezeichnung;
	private double preis;
	private int bestandSpieler;
	private int maxBestand;

	Addon(int idnummer, String bezeichnung, double preis, int bestandSpieler, int maxBestand) {
		this.idnummer = idnummer;
		this.bezeichnung = bezeichnung;
		this.preis = preis;
		this.bestandSpieler = bestandSpieler;
		this.maxBestand = maxBestand;
	}

	public void kaufen(int anzahl) {
		int x = bestandSpieler + anzahl;
		if (x >= maxBestand) {
			x = maxBestand;
		}
		setBestandSpieler(x);
	}

	public void verbrauchen(int anzahl) {
		int x = bestandSpieler - anzahl;
		if (x < 0) {
			x = 0;
		}
		setBestandSpieler(x);
	}

	public double getGesamtwert() {
		double x = this.preis * this.bestandSpieler;
		return x;
	}

	public int getIdnummer() {
		return idnummer;
	}

	public void setIdnummer(int idnummer) {
		this.idnummer = idnummer;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public int getBestandSpieler() {
		return bestandSpieler;
	}

	public void setBestandSpieler(int bestandSpieler) {
		this.bestandSpieler = bestandSpieler;
	}

	public int getMaxBestand() {
		return maxBestand;
	}

	public void setMaxBestand(int maxBestand) {
		this.maxBestand = maxBestand;
	}

}
