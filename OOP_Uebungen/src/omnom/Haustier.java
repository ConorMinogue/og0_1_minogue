package omnom;

public class Haustier {

	// Attribute
	private int hunger;
	private int muede;
	private int zufrieden;
	private int gesund;
	private String name;

	// Methoden - Konstruktor
	Haustier() {
		hunger = 100;
		muede = 100;
		zufrieden = 100;
		gesund = 100;
	}

	Haustier(String name) {
		hunger = 100;
		muede = 100;
		zufrieden = 100;
		gesund = 100;
		this.name = name;
	}

	// Methoden - Getter & Setter
	public int getHunger() {
		return hunger;
	}

	public void setHunger(int hunger) {
		if (hunger >= 0 && hunger <= 100) {
			this.hunger = hunger;
		}
	}

	public int getMuede() {
		return muede;
	}

	public void setMuede(int muede) {
		if (muede >= 0 && muede <= 100) {
			this.muede = muede;
		}
	}

	public int getZufrieden() {
		return zufrieden;
	}

	public void setZufrieden(int zufrieden) {
		if (zufrieden >= 0 && zufrieden <= 100) {
			this.zufrieden = zufrieden;
		}
	}

	public int getGesund() {
		return gesund;
	}

	public void setGesund(int gesund) {
		if (gesund >= 0 && gesund <= 100) {
			this.gesund = gesund;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// Methoden - Rest
	public void fuettern(int anzahl) {
		if (anzahl >= 0 && anzahl <= 100) {
			hunger += anzahl;
		}
		if(hunger>100) {
			hunger = 100;
		}
	}

	public void schlafen(int dauer) {
		if (dauer >= 0 && dauer <= 100) {
			muede += dauer;
		}
		if(muede>100) {
			muede = 100;
		}
	}

	public void spielen(int dauer) {
		if (dauer >= 0 && dauer <= 100) {
			zufrieden += dauer;
		}
		if(zufrieden>100) {
			zufrieden = 100;
		}
	}

	public void heilen() {
		gesund = 100;
	}
}
