package merging;

public class MergeSort {

	public static int[] zahlen = { 9, 13, 8, 4, 12, 11, 3, 15, 1, 14, 7, 6, 5, 2, 10 };
	
	// Alternatives sortiertes Array zum Testen
	//public static int[] zahlen = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
	
	// Zweites alternative sortiertes Array zum Test
	//public static int[] zahlen = { 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
	
	public int vergleiche = 0;
	public int vertauschungen = 0;

	public int[] sort(int links, int rechts) {

		if (links < rechts) {
			vertauschungen++;
			int q = (links + rechts) / 2;

			sort(links, q);
			sort(q + 1, rechts);
			merge(links, q, rechts);
			
			System.out.print("  ");
			for (int i = 0; i < zahlen.length; i++) {
				System.out.print(zahlen[i] + " ");
			}
			System.out.println();
		}
		return zahlen;
	}

	private void merge(int links, int q, int rechts) {
		int[] tempArray = new int[zahlen.length];
		int i, j;
		for (i = links; i <= q; i++) {
			tempArray[i] = zahlen[i];
		}
		for (j = q + 1; j <= rechts; j++) {
			tempArray[rechts + q + 1 - j] = zahlen[j];
		}
		i = links;
		j = rechts;
		for (int k = links; k <= rechts; k++) {
			if (tempArray[i] <= tempArray[j]) {
				zahlen[k] = tempArray[i];
				i++;
			} else {
				zahlen[k] = tempArray[j];
				j--;
			}
		vergleiche++;
		}
	}

	public static void main(String[] args) {
		
		MergeSort ms = new MergeSort();
		
		// Ausgabe des ursprünglichen Arrays
		System.out.print("{ ");
		for (int i = 0; i < zahlen.length; i++) {
			System.out.print(zahlen[i] + " ");
		}
		System.out.println("}");
		
		int[] arr = ms.sort(0, zahlen.length - 1);
		
		// Ausgabe des sortierten Arrays
		System.out.print("{ ");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println("}");
		
		System.out.println("Vergleiche: " + ms.vergleiche);
		System.out.println("Vertauschungen: " + ms.vertauschungen);
	}
}