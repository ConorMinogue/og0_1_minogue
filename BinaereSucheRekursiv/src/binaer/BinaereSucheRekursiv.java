package binaer;

public class BinaereSucheRekursiv {

	public int binaereSuche(int zahlen[], int links, int rechts, int gesuchteZahl) {
		if (rechts >= links && links < zahlen.length - 1) {
            int mitte = links + (rechts - links) / 2;
            if (zahlen[mitte] == gesuchteZahl)
                return mitte;
            if (zahlen[mitte] > gesuchteZahl)
                return binaereSuche(zahlen, links, mitte - 1, gesuchteZahl);
            return binaereSuche(zahlen, mitte + 1, rechts, gesuchteZahl);
        }
        return -1;
	}
	
    public static void main(String args[]) {
    	BinaereSucheRekursiv suche = new BinaereSucheRekursiv();
        int zahlen[] = { 1, 2, 3, 4, 5 };
        int rechts = zahlen.length;
        int gesuchteZahl = 2;
        int position = suche.binaereSuche(zahlen, 0, rechts - 1, gesuchteZahl);
        if (position == -1)
            System.out.println("Element wurde nicht gefunden.");
        else
            System.out.println("Element gefunden an der Indexstelle: " + position);
    }
}
