package sortier_verfahren;

import java.util.Arrays;

public class BubbleSortTest {
	public static void main(String[] args) {
		BubbleSort bs = new BubbleSort();

		long[] zahlenlisteAvrg = { 5, 4, 8, 3, 9, 7, 6, 2, 1 };
		long[] zahlenlisteBest = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		long[] zahlenlisteWorst = { 9, 8, 7, 6, 5, 4, 3, 2, 1 };

		// ------------------------
		// ----- Average Case -----
		// ------------------------
			// Vor Sortierung
			System.out.println(Arrays.toString(zahlenlisteAvrg));
	
			// Sortierung
			bs.sortiere(zahlenlisteAvrg);
	
			// Nach Sortierung
			System.out.println(Arrays.toString(zahlenlisteAvrg));
			System.out.println("Anzahl der Vertauschungen: " + bs.getVertauschungen());
			System.out.println("Anzahl der Überprüfungen: " + bs.getPruefungen());

		// ---------------------
		// ----- Best case -----
		// ---------------------
			bs.setPruefungen(0);
			bs.setVertauschungen(0);
			System.out.println();
			System.out.println("----- Best Case -----");
			// Vor Sortierung
			System.out.println(Arrays.toString(zahlenlisteBest));
	
			// Sortierung
			bs.sortiere(zahlenlisteBest);
	
			// Nach Sortierung
			System.out.println(Arrays.toString(zahlenlisteBest));
			System.out.println("Anzahl der Vertauschungen: " + bs.getVertauschungen());
			System.out.println("Anzahl der Überprüfungen: " + bs.getPruefungen());

		// ----------------------
		// ----- Worst case -----
		// ----------------------
			bs.setPruefungen(0);
			bs.setVertauschungen(0);
			System.out.println();
			System.out.println("----- Worst Case -----");
			// Vor Sortierung
			System.out.println(Arrays.toString(zahlenlisteWorst));
	
			// Sortierung
			bs.sortiere(zahlenlisteWorst);
	
			// Nach Sortierung
			System.out.println(Arrays.toString(zahlenlisteWorst));
			System.out.println("Anzahl der Vertauschungen: " + bs.getVertauschungen());
			System.out.println("Anzahl der Überprüfungen: " + bs.getPruefungen());
	}
}
