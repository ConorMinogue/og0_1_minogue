package sortier_verfahren;

public class BubbleSort {
	private long vertauschungen = 0;
	private long pruefungen = 0;

	public void sortiere(long[] zahlenliste) {
		int temp = 0;

		for (int n = zahlenliste.length - 1; n >= 1; n--) {
			for (int i = 0; i < n; i++) {
				if (zahlenliste[i] > zahlenliste[i + 1]) {
					temp = (int) zahlenliste[i];
					zahlenliste[i] = zahlenliste[i + 1];
					zahlenliste[i + 1] = temp;
					inkrementVertauschungen();
				}
				inkrementPruefungen();
			}
			System.out.println(array2str(zahlenliste));
		}

	}

	private void inkrementPruefungen() {
		pruefungen++;
	}

	public long getPruefungen() {
		return this.pruefungen;
	}

	public void setPruefungen(long pruefungen) {
		this.pruefungen = pruefungen;
	}

	private void inkrementVertauschungen() {
		vertauschungen++;
	}

	public long getVertauschungen() {
		return this.vertauschungen;
	}

	public void setVertauschungen(long vertauschungen) {
		this.vertauschungen = vertauschungen;
	}

	public static String array2str(long[] array) {
		String result = "";

		for (int i = 0; i < array.length; i++) {

			result = result + array[i] + "  ";

		}
		return result;
	}
}
