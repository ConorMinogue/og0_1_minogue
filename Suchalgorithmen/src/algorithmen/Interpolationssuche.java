package algorithmen;

public class Interpolationssuche {

	long n;
	private long[] werte;
	int versuche = 0;

	public void arrayfill() {
		werte = new long[(int) n];
		long j = 2;
		for (long i = 0l; i < werte.length; i++) {
			werte[(int) i] = j;
			j += 2;
		}
	}

	public long getN() {
		return n;
	}

	public void setN(long n) {
		this.n = n;
	}

	public long getWert(long stelle) {
		return werte[(int) stelle];
	}

	public long[] getWerte() {
		return werte;
	}

	public void setWerte(long[] werte) {
		this.werte = werte;
	}

	public long interpolation(long von, long bis, long x) {
		if (werte[(int) von] < werte[(int) bis]) {
			long t = von + ((bis - von) * ((x - werte[(int) von]) / (werte[(int) bis] - werte[(int) von])));
			if (x == werte[(int) t]) {
				return t;
			} else if (x < werte[(int) t]) {
				return interpolation(von, t - 1, x);
			} else {
				return interpolation(t + 1, bis, x);
			}

		} else if (x == werte[(int) von]) {
			return von;
		} else {
			return -1l;
		}

	}
	
	public long interpolationVersuche(long von, long bis, long x) {
		if (werte[(int) von] < werte[(int) bis]) {
			versuche++;
			long t = von + ((bis - von) * ((x - werte[(int) von]) / (werte[(int) bis] - werte[(int) von])));
			if (x == werte[(int) t]) {
				return versuche;
			} else if (x < werte[(int) t]) {
				return interpolationVersuche(von, t - 1, x);
			} else {
				return interpolationVersuche(t + 1, bis, x);
			}

		} else if (x == werte[(int) von]) {
			return versuche;
		} else {
			return versuche;
		}

	}
}
