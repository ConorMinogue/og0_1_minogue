package algorithmen;

import java.util.Scanner;

public class TestSuchalgorithmus {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		Interpolationssuche intsuche = new Interpolationssuche();
		
		intsuche.setN(15000000l);
		intsuche.arrayfill();
	
		System.out.println("Array mit " + intsuche.getN() + " Werten initialisiert!");

		System.out.println("Gr��er Wert: " + intsuche.getWert(intsuche.getN()-1));
		System.out.println("Gebe eine Zahl ein, welche im Array gesucht werden soll:");
		long x = scan.nextLong();
		

		double zeitStart = System.currentTimeMillis();
		long ergebnis = intsuche.interpolation(0, intsuche.getWerte().length-1, x);
		System.out.println("Die Zahl befindet sich an der Stelle: " + ergebnis);
		double zeitStopp = System.currentTimeMillis();
		System.out.println("Zeit: " + (zeitStopp - zeitStart) + "ms.");
		
		long versuche = intsuche.interpolationVersuche(0, intsuche.getWerte().length-1, x);
		System.out.println("Gebrauchte Versuche: " + versuche);
		
		
		scan.close();
	}

}
