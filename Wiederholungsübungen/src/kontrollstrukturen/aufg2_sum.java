package kontrollstrukturen;

import java.util.Scanner;

public class aufg2_sum {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);
		System.out.println("Gib eine Zahl ein: ");
		int n = myScanner.nextInt();
		int zaehler = 2;
		int zahl = 0;
		
		for (int i = 0; i < n; i++) {
			zahl += zaehler;
			zaehler+=2;
			System.out.println(zahl);
		}
		
		System.out.println("Deine Endzahl betr�gt: " + zahl);
		
		myScanner.close();
	}

}
