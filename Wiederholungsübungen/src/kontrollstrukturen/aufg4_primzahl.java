package kontrollstrukturen;

import java.util.Scanner;

public class aufg4_primzahl {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Willkommen beim Primzahlrechner!");
		System.out.println("Gebe eine Zahl ein: ");
		long x = myScanner.nextLong();

		boolean primzahl = true;

		for (int i = 2; i < (x / 2); i++) {
			long y = x % i;
			System.out.println(x + " geteilt durch " + i + " Rest: " + y);
			if (y == 0) {
				primzahl = false;
				System.out.println("Keine Primzahl");
				break;
			}
		}

		if (primzahl) {
			System.out.println("Die Zahl ist eine Primzahl");
		}
		myScanner.close();

	}

}
