package kontrollstrukturen;

import java.util.Scanner;

public class aufg1_grosshaendler01 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		double kosten = 12.75;

		System.out.println("Willkommmen!");
		System.out.println("Der Preis f�r eine Maus betr�gt: " + kosten + "�");
		System.out.println("Wie viele M�use m�chten Sie bestellen?");

		int anzahl = myScanner.nextInt();
		double preis = anzahl * kosten;

		if (anzahl < 10) {
			preis += 10;
		}

		preis = preis * 1.18;

		System.out.println("Ihre Rechnung betr�gt: " + preis);
		
		myScanner.close();
	}

}
