package kontrollstrukturen;

import java.util.Scanner;

public class aufg3_bmi {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);

		System.out.println("Geschlecht? (m/w)");
		char geschlecht = myScanner.next().charAt(0);

		System.out.println("Gewicht? (In kg)");
		int gewicht = myScanner.nextInt();

		System.out.println("Groesse? (In cm)");
		double groesseCM = myScanner.nextDouble();
		double groesseM = groesseCM / 100;
		System.out.println(groesseM);
		double groesseM2 = groesseM * groesseM;
		System.out.println(groesseM2);

		double bmi = gewicht / groesseM2;

		System.out.println("Dein BMI beträgt: " + bmi);
		System.out.println("Deine Gewichtsklasse ist: ");

		switch (geschlecht) {
		case 'm':
			if (bmi>25) {
				System.out.println("Übergewichtig");	
			} else if (bmi<20) {
				System.out.println("Untergewichtig");
			} else {
				System.out.println("Normalgewichtig");
			}
			break;
		case 'w':
			if (bmi>24) {
				System.out.println("Übergewichtig");	
			} else if (bmi<19) {
				System.out.println("Untergewichtig");
			} else {
				System.out.println("Normalgewichtig");
			}
			break;
		default:
			break;
		}

		myScanner.close();
	}

}
