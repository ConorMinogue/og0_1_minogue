package arrays;

public class aufg2_ArraysDurchlaufen {

	public static void main(String[] args) {
		
		int[] zahlen = {1, 6, 3, 7, 2, 2, 4, 8};
		
		for (int i = 0;  i < zahlen.length;  i++)  {
			System.out.println ( zahlen[i] );
			}
		
		System.out.println("-----");
		
		for (int i = 1;  i < zahlen.length / 2;  i++)  {
			System.out.println ( zahlen[i] );
			}
		
		System.out.println("-----");
		
		for (int i= zahlen.length -1;  i >= 0;  i--)  {
			System.out.println ( zahlen[i] );
		}
		
		System.out.println("-----");
		
		for (int i= 1;  i<=zahlen.length;  i*=2)  {
			System.out.println ( zahlen[i] );
			}
	}

}
