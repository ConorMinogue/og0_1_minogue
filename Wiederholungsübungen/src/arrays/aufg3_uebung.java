package arrays;

public class aufg3_uebung {

	public static void main(String[] args) {
		int[] zahlen;
		
		zahlen = new int[100];
		
		for (int i = 0; i < zahlen.length; i++) {
			System.out.print(zahlen[i] + ", ");
		}
		System.out.println("");
		System.out.println("-----");
		
		
		for (int i = 0; i < zahlen.length; i++) {
			zahlen[i] = (i+1);
		}
		
		System.out.println(zahlen[89]);
		System.out.println("-----");
		
		zahlen[49] = 1060;
		zahlen[0] = 2020;
		zahlen[99] = 2020;
		
		for (int i = 0; i < zahlen.length; i++) {
			System.out.print(zahlen[i]+ ", ");
		}
		System.out.println("");
		System.out.println("-----");
		
		int durchschnitt = 0;
		for (int i = 0; i < zahlen.length; i++) {
			durchschnitt += zahlen[i];
		}
		durchschnitt = durchschnitt/zahlen.length;
		System.out.println(durchschnitt);
	}

}
