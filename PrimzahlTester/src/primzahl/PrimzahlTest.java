package primzahl;

import java.util.Scanner;

public class PrimzahlTest {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Primzahl pz1 = new Primzahl();

		System.out.println("Willkommen beim Primzahl-Tester!");
		System.out.println("Gebe eine Zahl ein: ");

		long zahl = scan.nextLong();
		double zeitStart = System.currentTimeMillis();
		System.out.println(pz1.isPrimzahl(zahl));
		double zeitStopp = System.currentTimeMillis();

		System.out.println("Berechnungszeit: " + (zeitStopp - zeitStart) + "ms.");
		System.out.println("Berechnungszeit: " + ((zeitStopp - zeitStart) / 1000) + "s.");

		scan.close();
	}
}
