package primzahl;

public class PrimzahlFinalTest {

	public static void main(String[] args) {
		Primzahl pz1 = new Primzahl();
		
		long[] zahlen = {6250171l, 76250203l, 843250223l, 2876250203l, 32876250193l};

		for (int i = 0; i < zahlen.length; i++) {
		System.out.println("Zahl " + (i+1) + ": " + zahlen[i]);
		double zeitStart = System.currentTimeMillis();
		System.out.println(pz1.isPrimzahl(zahlen[i]));
		double zeitStopp = System.currentTimeMillis();
		
		System.out.println("Zeit: " + (zeitStopp - zeitStart) + "ms.");
		System.out.println("Zeit: " + ((zeitStopp - zeitStart) / 1000) + "s.");
		System.out.println("-----");
		}
	}
}