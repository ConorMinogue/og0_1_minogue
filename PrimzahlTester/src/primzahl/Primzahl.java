package primzahl;

public class Primzahl {

	public boolean isPrimzahl(long zahl) {
		boolean primzahl = true;

		// Abfrage auf Zahlen kleiner und gleich 1
		if (zahl <= 1) {
			primzahl = false;
		}

		// Abfrage auf Prim-Zustand f�r Zahlen gr��er als 1
		if (primzahl == true) {
			for (int i = 2; i <= (zahl / 2); i++) {
				if ((zahl % i) == 0) {
					primzahl = false;
					break;
				}
			}
		}
		return primzahl;
	}
}
