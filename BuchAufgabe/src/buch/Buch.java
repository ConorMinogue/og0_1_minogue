package buch;

public class Buch implements Comparable<Buch> {

	private String autor;
	private String titel;
	private String isbnNummer;

	public Buch(String autor, String titel, String isbnNummer) {
		super();
		this.autor = autor;
		this.titel = titel;
		this.isbnNummer = isbnNummer;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getIsbnNummer() {
		return isbnNummer;
	}

	public void setIsbnNummer(String isbnNummer) {
		this.isbnNummer = isbnNummer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((isbnNummer == null) ? 0 : isbnNummer.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Buch other = (Buch) obj;
		if (isbnNummer == null) {
			if (other.isbnNummer != null)
				return false;
		} else if (!isbnNummer.equals(other.isbnNummer))
			return false;
		return true;
	}

	public String toString() {
		return "[ Autor: " + this.autor + ", Titel: " + this.titel + ", ISBN-Nummer:" + this.isbnNummer + " ]";
	}

	public int compareTo(Buch b) {
		return this.isbnNummer.compareTo(b.getIsbnNummer());

	}
}
