package junit;

import static org.junit.Assert.*;
import org.junit.Test;
import work.KleinstesGemeinsamesVielfaches;

public class Test_1 {
	
	@Test
	public void testNull1() {
		assertEquals(0, KleinstesGemeinsamesVielfaches.kgV(0, 0));
	}
	
	@Test
	public void testNull2() {
		assertEquals(0, KleinstesGemeinsamesVielfaches.kgV(5, 0));
	}
	
	@Test
	public void testNull3() {
		assertEquals(0, KleinstesGemeinsamesVielfaches.kgV(0, 5));
	}
	
	@Test
	public void testGleich1() {
		assertEquals(3, KleinstesGemeinsamesVielfaches.kgV(3, 3));
	}
	
	@Test
	public void testGleich2() {
		assertEquals(18, KleinstesGemeinsamesVielfaches.kgV(18, 18));
	}
	
	@Test
	public void testGleich3() {
		assertEquals(77, KleinstesGemeinsamesVielfaches.kgV(77, 77));
	}
	
	@Test
	public void test1() {
		assertEquals(6, KleinstesGemeinsamesVielfaches.kgV(3, 6));
	}
	
	@Test
	public void test2() {
		assertEquals(72, KleinstesGemeinsamesVielfaches.kgV(36, 72));
	}
	
	@Test
	public void test3() {
		assertEquals(1296, KleinstesGemeinsamesVielfaches.kgV(144, 1296));
	}
	
	@Test
	public void test4() {
		assertEquals(80, KleinstesGemeinsamesVielfaches.kgV(5, 16));
	}
	
	@Test
	public void test5() {
		assertEquals(37682, KleinstesGemeinsamesVielfaches.kgV(166, 227));
	}
	
	@Test
	public void test6() {
		assertEquals(5809152, KleinstesGemeinsamesVielfaches.kgV(1024, 5673));
	}
	
	@Test
	public void test7() {
		assertEquals(27972, KleinstesGemeinsamesVielfaches.kgV(28, 999));
	}
	

	@Test
	public void test8() {
		assertEquals(272692728, KleinstesGemeinsamesVielfaches.kgV(9999, 27272));
	}
	
	@Test
	public void test9() {
		assertEquals(252265040, KleinstesGemeinsamesVielfaches.kgV(14780, 68272));
	}
	
	@Test
	public void test10() {
		assertEquals(513830025, KleinstesGemeinsamesVielfaches.kgV(28989, 53175));
	}
	
	@Test
	public void testNegativ1() {
		assertEquals(27, KleinstesGemeinsamesVielfaches.kgV(-3, 27));
	}
	
	@Test
	public void testNegativ2() {
		assertEquals(3264, KleinstesGemeinsamesVielfaches.kgV(17, -192));
	}
	
	@Test
	public void testNegativ3() {
		assertEquals(309217, KleinstesGemeinsamesVielfaches.kgV(-373, -829));
	}

}
