package work;

public class DecimalToRoman {

	/**
	 * wandelt eine beliebige Zahl zwischen 1-3499 in eine r�mische Zahl um
	 * 
	 * @param zahl
	 * @return
	 */
	public static String dezimalToRoman(int zahl) {
		if (zahl < 1 || zahl > 3499)
			return "Au�erhalb des zul�ssigen Bereichs";
		String ergebnis = "";
		int count = 0;

		if (zahl >= 900) {
			// Anzahl der M
			count = zahl / 1000;
			// Sonderfall x9xx
			while (count-- >= 1)
				ergebnis += "M";

			if (zahl % 1000 >= 900)
				ergebnis += "CM";
		}

		// D?
		if (zahl % 1000 >= 500 && zahl % 1000 < 900)
			ergebnis += "D";

		// CD
		if (zahl % 1000 >= 400 && zahl % 1000 < 500)
			ergebnis += "CD";

		if (zahl >= 90 && zahl != 500) {

			// Anzahl der C
			count = zahl / 100 % 5;
			// Sonderfall x4xx u. x9xx
			if (count != 4) {
				// Sonderfall x9x
				while (count-- >= 1)
					ergebnis += "C";
			}
			if (zahl % 100 >= 90)
				ergebnis += "XC";

		}

		// L?
		if (zahl % 100 >= 50 && zahl % 100 < 90)
			ergebnis += "L";

		// CD
		if (zahl % 100 >= 40 && zahl % 100 < 50)
			ergebnis += "XL";

		if (zahl >= 9) {
			// Anzahl der X
			count = zahl / 10 % 5;
			// Sonderfall x4xx u. x9xx
			if (count != 4) {
				// Sonderfall x9
				while (count-- >= 1)
					ergebnis += "X";
			}
			if (zahl % 10 >= 9)
				ergebnis += "IX";

		}

		// V?
		if (zahl % 10 >= 5 && zahl % 10 != 9)
			ergebnis += "V";

		// Anzahl der I
		count = zahl % 5;
		// Sonderfall 4

		if (zahl % 10 == 4)
			ergebnis += "IV";
		else if (zahl % 10 != 9)
			while (count-- > 0)
				ergebnis += "I";

		return ergebnis;
	}
}
