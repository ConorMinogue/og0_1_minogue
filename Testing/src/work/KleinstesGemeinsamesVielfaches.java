package work;

public class KleinstesGemeinsamesVielfaches {

	public static int kgV(int a, int b) {
		int a1 = Math.abs(a);
		int b1 = Math.abs(b);

		// Überprüfung ob mindestens eine Zahl = 0 ist
		if ((a1 == 0) || (b1 == 0)) {
			return 0; // Falls eine der Zahlen 0 ist, kgV = 0
		}

		// Überprüfung ob die Zahlen gleich sind
		if (a1 == b1) {
			return a1; // Die Zahlen sind gleich, kgV = a oder b
		}

		while (a1 != b1) {
			if (a1 > b1) {
				b1 += Math.abs(b);
			} else {
				a1 += Math.abs(a);
			}
			
		}
		return a1; // Der kgV der Zahlen
	}

}
