package git_Taschenrechner;

import java.util.Scanner;

public class TaschenrechnerTest {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		Taschenrechner ts = new Taschenrechner();

		int swValue;
		boolean wdh = true; // Parameter f�r Schleife

		while (wdh == true) { // Beginn der Taschenrechner-Schleife

			// Display menu graphics
			System.out.println("============================");
			System.out.println("|   MENU SELECTION DEMO    |");
			System.out.println("============================");
			System.out.println("| Options:                 |");
			System.out.println("|        1. Addieren       |");
			System.out.println("|        2. Subtrahieren   |");
			System.out.println("|        3. Multiplizieren |");
			System.out.println("|        4. Dividieren     |");
			System.out.println("|        5. Exit           |");
			System.out.println("============================");
			System.out.print(" Select option: ");
			swValue = myScanner.next().charAt(0);

			System.out.println("Gebe die erste Zahl ein: ");
			double zahl1 = myScanner.nextDouble();
			System.out.println("Gebe die zweite Zahl ein: ");
			double zahl2 = myScanner.nextDouble();

			// Switch construct
			switch (swValue) {
			case '1':
				System.out.println(zahl1 + " + " + zahl2 + " = " + ts.add(zahl1, zahl2));
				break;
			case '2':
				System.out.println(zahl1 + " - " + zahl2 + " = " + ts.sub(zahl1, zahl2));
				break;
			case '3':
				System.out.println(zahl1 + " * " + zahl2 + " = " + ts.mul(zahl1, zahl2));
				break;
			case '4':
				System.out.println(zahl1 + " / " + zahl2 + " = " + ts.div(zahl1, zahl2));
				break;
			case '5':
				System.out.println("Beende Taschenrecher...");
				wdh = false;
				break;
			default:
				System.out.println("Ung�ltige Eingabe");
				break; // This break is not really necessary

			} // Ende der Switch-Auswahl
			System.out.println("   ");

		} // Ende der Taschenrechner Schleife
		myScanner.close();
	}

}
