package arrlinklist;

import java.util.*;

public class ArrLinkList {
	
	public static void main(String[] args) {
		
		List<Integer> zahlenliste = new ArrayList<Integer>();
		// List<Integer> zahlenliste = new LinkedList<Integer>();
		
		zahlenliste.add(17); // 01
		zahlenliste.add(15); // 02
		zahlenliste.add(42); // 03
		zahlenliste.add(53); // 04
		zahlenliste.add(70); // 05
		zahlenliste.add(39); // 06
		zahlenliste.add(28); // 07
		zahlenliste.add(81); // 08
		zahlenliste.add(96); // 09
		zahlenliste.add(10); // 10
		zahlenliste.add(52); // 11
		zahlenliste.add(48); // 12
		zahlenliste.add(24); // 13
		zahlenliste.add(44); // 14
		zahlenliste.add(54); // 15
		
		System.out.println(zahlenliste);
		System.out.println();
		
		// add
		System.out.println("Add: ");
		System.out.println(zahlenliste.add(48));
		System.out.println();
		
		// size
		System.out.println("Size: ");
		System.out.println(zahlenliste.size());
		System.out.println();
		
		// remove
		System.out.println("Remove: ");
		System.out.println(zahlenliste.remove(3));
		System.out.println();
		
		// get
		System.out.println("Get: ");
		System.out.println(zahlenliste.get(3));
		System.out.println();
		
		// clear
		System.out.println("Clear: ");
		zahlenliste.clear();
		System.out.println(zahlenliste);
		System.out.println();
	}
	
}
